# Context

`ExecutionContext` for stackful coroutines and fibers

## Requirements

- x86-64
- Clang++

## Features

- Exceptions support
- Annotations for Address Sanitizer
- Annotations for Thread Sanitizer

## References

- [System V ABI / AMD64](https://www.uclibc.org/docs/psABI-x86_64.pdf)
- [ARMv8-A ISA](https://documentation-service.arm.com/static/613a2c38674a052ae36ca307)
