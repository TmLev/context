#pragma once

#include <wheels/memory/view.hpp>

namespace context {

wheels::ConstMemView ThisThreadStack();

}  // namespace context
